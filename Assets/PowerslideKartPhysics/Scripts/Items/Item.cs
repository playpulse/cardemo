﻿// Copyright (c) 2020 Justin Couch / JustInvoke

using UnityEngine;

namespace PowerslideKartPhysics
{
    //Class for different items to be used
    public abstract class Item : MonoBehaviour
    {
        public string itemName = "Item";

        //Called upon activation
        public virtual void Activate(ItemCastProperties props)
        {
        }

        //Called upon deactivation
        public virtual void Deactivate()
        {
        }
    }
}